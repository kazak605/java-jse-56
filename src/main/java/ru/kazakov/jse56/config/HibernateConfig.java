package ru.kazakov.jse56.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import ru.kazakov.jse56.model.Chat;
import ru.kazakov.jse56.model.Message;
import ru.kazakov.jse56.model.Users;


public class HibernateConfig {
    private static SessionFactory sessionFactory;

    private HibernateConfig() {
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) return sessionFactory;
        Configuration cfg = new Configuration();
        cfg.addAnnotatedClass(Users.class);
        cfg.addAnnotatedClass(Message.class);
        cfg.addAnnotatedClass(Chat.class);
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(cfg.getProperties())
                .build();
        sessionFactory = cfg.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

}
