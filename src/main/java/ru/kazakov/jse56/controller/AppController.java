package ru.kazakov.jse56.controller;

import lombok.extern.log4j.Log4j2;
import ru.kazakov.jse56.model.Chat;
import ru.kazakov.jse56.model.Message;
import ru.kazakov.jse56.model.Users;
import ru.kazakov.jse56.repository.ChatRepository;
import ru.kazakov.jse56.repository.MessageRepository;
import ru.kazakov.jse56.repository.UserRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Log4j2
public class AppController {

    private UserRepository userRepository = new UserRepository();
    private MessageRepository messageRepository = new MessageRepository();
    private ChatRepository chatRepository = new ChatRepository();

    private static Scanner scanner = new Scanner(System.in);
    private String cmd = "";
    private static final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss").withZone(ZoneId.systemDefault());

    private Users currentUser;
    private Chat currentChat;

    private LocalDateTime dateTime;

    public void process() {
        authorization();
        showChatList();
        messaging();
        while (!"EXIT".equalsIgnoreCase(cmd)) {
            saveMessage();
        }
    }

    private void saveMessage() {
        cmd = scanner.nextLine();
        if (cmd != null) {
            Message message = new Message();
            message.setMsgText(cmd);
            message.setMsgDate(LocalDateTime.now());
            message.setChat(currentChat);
            message.setUser(currentUser);
            currentChat.getMessages().add(message);
            messageRepository.save(message);
        }
    }

    private void authorization() {
        while (currentUser == null) {
            log.info("Enter login");
            String login = scanner.nextLine();
            log.info("Enter password");
            String password = scanner.nextLine();
            Optional<Users> authorization = userRepository.findByLogin(login, password);
            if (authorization.isPresent()) {
                currentUser = authorization.get();
            } else {
                log.info("Authorization failed");
            }
        }
    }

    private void showChatList() {
        log.info("Chat list:");
        List<Chat> chatList = chatRepository.findAll();
        if (chatList.isEmpty()) {
            log.info("Chats are not found");
            return;
        }
        int level = 1;
        for (Chat chat : chatList) {
            log.info(level + ". " + chat.getChatName());
            level++;
        }
    }

    private void messaging() {
        log.info("Enter to chat {}", currentChat.getChatName());
        cmd = scanner.nextLine();
        Optional<Chat> chat = chatRepository.findByName(cmd);
        if (chat.isEmpty()) {
            Chat chat1 = new Chat();
            chat1.setChatName(cmd);
            currentChat = chatRepository.save(chat1);
        }
        dateTime = LocalDateTime.now();
        startTimer();
    }

    private void startTimer() {
        Timer timer = new Timer();
        int begin = 0;
        int delay = 1000;
        timer.schedule(new TimerTask() {
            int counter = 0;
            @Override
            public void run() {
                checkNewMessage();
                counter++;
            }
        }, begin, delay);
    }

    private void checkNewMessage() {
        List<Message> messageList = messageRepository.findUnReadMessages(currentChat,
                currentUser, dateTime);
        messageList.forEach(
                message -> log.info(String.format("[User %s sent a message: %s] -> %s",
                        message.getUser().getLogin(),
                        message.getMsgDate().format(format),
                        message.getMsgText())
                )
        );
        dateTime = LocalDateTime.now();
    }


}
