package ru.kazakov.jse56.repository;


import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.kazakov.jse56.config.HibernateConfig;
import ru.kazakov.jse56.model.Chat;
import ru.kazakov.jse56.model.Message;
import ru.kazakov.jse56.model.Users;

import javax.persistence.TemporalType;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Log4j2
public class MessageRepository extends CommonRepository {

    private final SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public List<Message> findAll() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Message> result = session.createQuery("SELECT m FROM Message m", Message.class).getResultList();
        tx.commit();
        session.close();
        if (result.isEmpty()) {
            return Collections.emptyList();
        } else {
            return result;
        }
    }

    public List<Message> findUnReadMessages(Chat chat, Users user, LocalDateTime lastDate) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query<Message> query = session.createQuery("SELECT m FROM Message m WHERE m.chat = :chatId and m.user <> :userId and m.msgDate >= :msgDate", Message.class);
        query.setParameter("chatId", chat.getChatId());
        query.setParameter("userId", user.getUserId());
        query.setParameter("msgDate", lastDate, TemporalType.TIMESTAMP);
        List<Message> result = query.getResultList();
        tx.commit();
        session.close();
        return result;
    }

}
