package ru.kazakov.jse56.repository;

import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.kazakov.jse56.config.HibernateConfig;
import ru.kazakov.jse56.model.Chat;

import java.util.List;
import java.util.Optional;

@Log4j2
public class ChatRepository extends CommonRepository {

    private final SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public List<Chat> findAll() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Chat> result = session.createQuery("SELECT c FROM Chat c", Chat.class).getResultList();
        tx.commit();
        session.close();
        return result;

    }

    public Optional<Chat> findByName(String chatName) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<Chat> result;
        Query<Chat> query = session.createQuery("SELECT c FROM Chat c WHERE c.chatName = :chatName", Chat.class);
        query.setParameter("chatName", chatName);
        result = query.uniqueResultOptional();
        tx.commit();
        session.close();
        return result;
    }

}
