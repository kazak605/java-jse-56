package ru.kazakov.jse56.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.kazakov.jse56.config.HibernateConfig;

public class CommonRepository {

    private final SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public <T> T save(T entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(entity);
        tx.commit();
        session.close();

        return entity;
    }

}
