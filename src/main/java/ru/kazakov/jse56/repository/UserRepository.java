package ru.kazakov.jse56.repository;

import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.kazakov.jse56.config.HibernateConfig;
import ru.kazakov.jse56.model.Users;

import java.util.List;
import java.util.Optional;

@Log4j2
public class UserRepository extends CommonRepository {

    private final SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public List<Users> getAll() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Users> users = session.createQuery("SELECT c FROM Users c", Users.class).getResultList();
        tx.commit();
        session.close();
        return users;
    }

    public Optional<Users> findByLogin(String login, String password) {
        Optional<Users> result;
        try {
            Session session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            Query<Users> query = session.createQuery("SELECT c FROM Users c WHERE c.login = :login and c.password = :password", Users.class);
            query.setParameter("login", login.toUpperCase());
            query.setParameter("password", password.toUpperCase());
            if (query.getSingleResult() == null) {
                result = Optional.empty();
            } else {
                result = Optional.of(query.getSingleResult());
            }
            tx.commit();
            session.close();
        } catch (Exception e) {
            log.error(e.getMessage());
            result = Optional.empty();
        }
        return result;
    }

}
