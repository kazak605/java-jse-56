package ru.kazakov.jse56;


import ru.kazakov.jse56.controller.AppController;

public class App {

    public static void main(String[] args) {
        AppController appController = new AppController();
        appController.process();
    }
}
