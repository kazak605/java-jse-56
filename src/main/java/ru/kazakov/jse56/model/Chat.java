package ru.kazakov.jse56.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "chat")
public class Chat implements Serializable {

    @Id
    @Column(name = "chat_id", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer chatId;

    @Column(name = "chat_name", length = 50, nullable = false)
    private String chatName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "chat")
    private List<Message> messages = new ArrayList<>();

}
